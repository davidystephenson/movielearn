/* global createNetwork, confirm, movies, table, train, analyze, prompt, predict, alert, network:writable */

function likeMovie (title, movie) {
  const like = confirm(title)

  if (like) {
    movie.target = 1
  } else {
    movie.target = 0
  }

  return movie
}

function learn () {
  const ok = confirm('Can I learn about you?')

  if (ok) {
    for (const key in movies) {
      const movie = movies[key]

      console.log('movie test:', movie)

      movies[key] = likeMovie(key, movie)
    }
    table('User training', movies)

    network = createNetwork()
    train()
    analyze()
    test()
  }
}

function test () {
  const movie = {}

  movie.name = prompt('What movie do you want to predict?')
  console.log('Name:', movie.name)

  movie.year = prompt('What year was it made?')
  console.log('Year:', movie.year)

  movie.length = prompt('How many minutes long is it?')
  console.log('Length:', movie.length)

  const prediction = predict(network, movie)
  alert(prediction)

  const another = confirm('Test another movie?')

  if (another) {
    test()
  }
}

window.addEventListener('keydown', learn)

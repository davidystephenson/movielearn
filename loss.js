/* global predict, movies, table, madMax, getAverage network */

function predictMovies (network, movies) {
  for (const title in movies) {
    const movie = movies[title]

    const prediction = predict(network, movie)
    movie.prediction = prediction
  }

  return movies
}

const predictions = predictMovies(network, movies)
table('Predictions', predictions)

function getError (movie) {
  const difference = movie.target - movie.prediction
  const error = difference * difference

  return error
}

const madMaxError = getError(madMax)
console.log('Mad Max error:', madMaxError)

function getLoss (network, movies) {
  const predictions = predictMovies(network, movies)

  for (const title in predictions) {
    const movie = predictions[title]
    const error = getError(movie)

    movie.error = error
  }

  const loss = getAverage(predictions, 'error')

  return loss
}

const loss = getLoss(network, movies)
console.log('Loss:', loss)

// TODO minutes -> length

const movies = {
  madMax: { year: 2015, length: 120, target: 1 },
  lostInTranslation: { year: 2003, length: 102, target: 1 },
  jumper: { year: 2008, length: 88, target: 0 },
  godfather: { year: 1972, length: 177, target: 1 },
  getOut: { year: 2017, length: 104, target: 1 },
  westSideStory: { year: 1961, length: 152, target: 0 },
  wizardOfOz: { year: 1939, length: 101, target: 0 },
  backToTheFuture: { year: 1985, length: 116, target: 0 },
  casablanca: { year: 1942, length: 102, target: 1 },
  ghostbusters: { year: 1984, length: 105, target: 0 }
}

console.log('Movies')
console.table(movies)

function table (title, data) {
  console.log(title)
  console.table(data)
}

const madMax = movies.madMax
table('Mad Max', madMax)

let totalLength = 0
let movieCount = 0

for (const title in movies) {
  const movie = movies[title]

  totalLength = totalLength + movie.length
  movieCount = movieCount + 1
}

console.log('Total length:', totalLength)
console.log('Movie count:', movieCount)

const averageLength = totalLength / movieCount
console.log('Average length:', averageLength)

function getAverage (movies, key) {
  let total = 0
  let count = 0

  for (const title in movies) {
    const movie = movies[title]
    const value = movie[key]

    total = total + value

    count = count + 1
  }

  const average = total / count

  return average
}
const averageYear = getAverage(movies, 'year')
console.log('Average year:', averageYear)

/* global table, madMax, averageYear, averageLength */

function createNeuron () {
  return {
    a: 0,
    b: 0,
    bias: 0
  }
}

const neuron = createNeuron()
table('Neuron', neuron)

function createNetwork () {
  return {
    left: createNeuron(),
    right: createNeuron(),
    center: createNeuron()
  }
}

let network = createNetwork()

table('Network', network)

function sigma (x) {
  return 1 / (1 + Math.exp(-x))
}

function forward (neuron, a, b) {
  const feelingA = a * neuron.a
  const feelingB = b * neuron.b

  const opinion = feelingA + feelingB + neuron.bias
  const signal = sigma(opinion)

  return signal
}

const signal = forward(network.neuron1, madMax.year, madMax.length)
console.log('Signal:', signal)

function predict (network, movie) {
  const normalYear = movie.year - averageYear
  const normalLength = movie.length - averageLength

  const left = forward(network.left, normalYear, normalLength)
  const right = forward(network.right, normalYear, normalLength)
  const prediction = forward(network.center, left, right)

  return prediction
}

const prediction = predict(network, madMax)
console.log('Prediction:', prediction)

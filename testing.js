/* global getLoss, network, table */

console.log('Testing!')

const tests = {
  tron: { year: 1982, length: 96, target: 0 },
  matrix: { year: 1999, length: 136, target: 1 },
  citizenKane: { year: 1941, length: 119, target: 1 },
  interstellar: { year: 2014, length: 169, target: 1 },
  rashomon: { year: 1950, length: 88, target: 0 }
}

const testLoss = getLoss(network, tests)
table('Tests:', tests)
console.log('Test loss:', testLoss)

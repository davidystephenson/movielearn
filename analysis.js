/* global table, predict, network */

const data = {
  90: { 1940: 0, 1970: 0, 2000: 0, 2030: 0 },
  120: { 1940: 0, 1970: 0, 2000: 0, 2030: 0 },
  150: { 1940: 0, 1970: 0, 2000: 0, 2030: 0 },
  180: { 1940: 0, 1970: 0, 2000: 0, 2030: 0 }
}

table('Data', data)

function analyze (runtime) {
  for (const length in data) {
    const row = data[length]

    for (const year in row) {
      const imaginary = {
        length: length,
        year: year
      }
      const prediction = predict(network, imaginary)

      row[year] = prediction
    }
  }

  table('Analysis', data)
}

analyze()

/* global movies, getLoss, table, createNetwork */
/* global network:writable */

function mutate (number) {
  const chaos = Math.random()
  const upOrDown = chaos - 0.5

  return number + upOrDown
}

const mutated = mutate(0)
console.log('Mutated:', mutated)

function reproduce (network) {
  const offspring = createNetwork()

  for (const position in network) {
    const neuron = network[position]

    for (const axon in neuron) {
      const weight = neuron[axon]
      const mutated = mutate(weight)

      const offspringNeuron = offspring[position]
      offspringNeuron[axon] = mutated
    }
  }

  return offspring
}

const reproduced = reproduce(network)
console.log('Reproduced:', reproduced)

function train () {
  console.log('Training!')

  let age = 0
  while (age < 100000) {
    const offspring = reproduce(network)

    const loss = getLoss(network, movies)
    const offspringLoss = getLoss(offspring, movies)

    if (offspringLoss < loss) {
      network = offspring
      console.log('Loss:', loss)

      age = 0
    } else {
      age = age + 1
    }
  }

  console.log('Trained!')
  table('Network', network)

  const loss = getLoss(network, movies)
  table('Movies', movies)
  console.log('Loss:', loss)
}

train()
